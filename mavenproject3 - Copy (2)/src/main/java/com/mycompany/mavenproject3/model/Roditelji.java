/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.model;

/**
 *
 * @author Grupa1
 */
public class Roditelji {
    
    private int id;
    private int korisnik_id;
    private int ucenik_id;

    public Roditelji() {
    }
    
    public Roditelji( int korisnik_id, int ucenik_id) {
        this.korisnik_id = korisnik_id;
        this.ucenik_id = ucenik_id;
    }

    public Roditelji(int id, int korisnik_id, int ucenik_id) {
        this.id = id;
        this.korisnik_id = korisnik_id;
        this.ucenik_id = ucenik_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getKorisnik_id() {
        return korisnik_id;
    }

    public void setKorisnik_id(int korisnik_id) {
        this.korisnik_id = korisnik_id;
    }

    public int getUcenik_id() {
        return ucenik_id;
    }

    public void setUcenik_id(int ucenik_id) {
        this.ucenik_id = ucenik_id;
    }

    @Override
    public String toString() {
        return "Roditelji{" + "id=" + id + ", korisnik_id=" + korisnik_id + ", ucenik_id=" + ucenik_id + '}';
    }
    
    
    
}
