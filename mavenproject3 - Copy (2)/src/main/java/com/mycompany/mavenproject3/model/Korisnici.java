/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.model;

/**
 *
 * @author Grupa1
 */
public class Korisnici {
    
    private int id;
    private String ime;
    private String prezime;
    private String korisnik;
    private String lozinka;
    private String email;
    private String uloga;

    public Korisnici() {
    }
    
     public Korisnici( String ime, String prezime, String korisnik, String lozinka, String email, String uloga) {

        this.ime = ime;
        this.prezime = prezime;
        this.korisnik = korisnik;
        this.lozinka = lozinka;
        this.email = email;
        this.uloga = uloga;
    }


    public Korisnici(int id, String ime, String prezime, String korisnik, String lozinka, String email, String uloga) {
        this.id = id;
        this.ime = ime;
        this.prezime = prezime;
        this.korisnik = korisnik;
        this.lozinka = lozinka;
        this.email = email;
        this.uloga = uloga;
    }

   

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(String korisnik) {
        this.korisnik = korisnik;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUloga() {
        return uloga;
    }

    public void setUloga(String uloga) {
        this.uloga = uloga;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    @Override
    public String toString() {
        return "Korisnici{" + "id=" + id + ", ime=" + ime + ", prezime=" + prezime + ", korisnik=" + korisnik + ", lozinka=" + lozinka + ", email=" + email + ", uloga=" + uloga + '}';
    }
    
    
          
    
}
