/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



@Entity
public class Administratori extends Korisnici {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int korisnik_id;

    public Administratori() {
    }

    public Administratori(int korisnik_id, String ime, String prezime, String korisnik, String lozinka, String email, String uloga) {
        super(ime, prezime, korisnik, lozinka, email, uloga);
        this.korisnik_id = korisnik_id;
    }

    public Administratori(int id, int korisnik_id, String ime, String prezime, String korisnik, String lozinka, String email, String uloga) {
        super(ime, prezime, korisnik, lozinka, email, uloga);
        this.id = id;
        this.korisnik_id = korisnik_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getKorisnik_id() {
        return korisnik_id;
    }

    public void setKorisnik_id(int korisnik_id) {
        this.korisnik_id = korisnik_id;
    }

    @Override
    public String toString() {
        return "Administratori{" + "id=" + id + ", korisnik_id=" + korisnik_id + '}';
    }

    
}
