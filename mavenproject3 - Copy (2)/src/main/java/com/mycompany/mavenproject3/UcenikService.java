/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3;

import com.mycompany.mavenproject3.model.Ucenik;

/**
 *
 * @author Grupa1
 */
public interface UcenikService {
     public Iterable<Ucenik> listAllUsers();

	   public Ucenik getUserById(long id);

	   public Ucenik saveUser(Ucenik user);
	    
	   public void deleteUser(long id);
}
