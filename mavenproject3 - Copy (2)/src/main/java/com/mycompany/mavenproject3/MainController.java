package com.mycompany.mavenproject3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

//import hello.User;
//import hello.UserRepository;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;
import org.springframework.ui.Model;
//import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller    // This means that this class is a Controller
//@RequestMapping(path = "/demo") // This means URL's start with /demo (after Application path)
public class MainController {

//    @Autowired
//    private UcenikRepository ucenikRepository;
    
    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
	public String index(Model model) {
		
		return "index";
	}

@RequestMapping(value = { "/admin" }, method = RequestMethod.GET)
	public String admin(Model model) {
		
		return "admin";
	}
        @RequestMapping(value = { "/direktor" }, method = RequestMethod.GET)
	public String direktor(Model model) {
		
		return "direktor";
	}
        @RequestMapping(value = {  "/roditelj" }, method = RequestMethod.GET)
	public String roditelj(Model model) {
		
		return "roditelj";
	}
        @RequestMapping(value = {  "/ucitelj" }, method = RequestMethod.GET)
	public String ucitelj(Model model) {
		
		return "ucitelj";
	}
        @RequestMapping(value = { "/komunikacija" }, method = RequestMethod.GET)
	public String komunikacija(Model model) {
		
		return "komunikacija";
	}


}

    


