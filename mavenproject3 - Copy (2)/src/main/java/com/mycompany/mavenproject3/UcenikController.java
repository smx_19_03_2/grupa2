/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3;


import com.mycompany.mavenproject3.model.Ucenik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Grupa1
 */
@Controller 
@RequestMapping(path="/ucenik")
public class UcenikController {
       // This means that this class is a Controller
//@RequestMapping(path = "/demo") // This means URL's start with /demo (after Application path)


   @Autowired
    private UcenikRepository ucenikRepository;
   
  
   private UcenikService ucenikService;
   
   @Autowired
	public void setUcenikService(ImplUcenikService ucenikService) {
		this.ucenikService = ucenikService;
        }

    @PostMapping(path = "/dodaj")
    public @ResponseBody
    String addNewProba(@RequestParam String ime, @RequestParam String prezime) {
        Ucenik u = new Ucenik();
        u.setIme(ime);
        u.setPrezime(prezime);
        ucenikRepository.save(u);
        return "Snimljen Učenik !";

    }
//    @GetMapping(path = "/allUcenik")
//    public @ResponseBody
//    Iterable<Ucenik> getAllProba() {
//        // This returns a JSON or XML with the users
//        return ucenikRepository.findAll();
//    }
    
   
	/*@RequestMapping(value ="/allUcenik", method=RequestMethod.POST)
        @ResponseBody
    public ModelAndView allUcenik() {
        ModelAndView model = new ModelAndView("allUcenik");
        model.addObject("list", ucenikService.listAllUsers());
        return model;
    }
*/
    @GetMapping(path="/allUcenik")
    public @ResponseBody Iterable<Ucenik> getAllUsers() {
        // This returns a JSON or XML with the users
        return ucenikRepository.findAll();}
}

