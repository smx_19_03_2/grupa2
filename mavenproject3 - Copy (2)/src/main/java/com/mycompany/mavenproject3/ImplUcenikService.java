/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3;

import com.mycompany.mavenproject3.model.Ucenik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Grupa1
 */
@Service
public class ImplUcenikService implements UcenikService{
    
    private UcenikRepository ucenikRepo;
    
    @Autowired
	public void setUcenikRepo(UcenikRepository ucenikRepo) {
		this.ucenikRepo = ucenikRepo;
	}

    @Override
    public Iterable<Ucenik> listAllUsers() {
       
		return ucenikRepo.findAll();
	
    }

    @Override
    public Ucenik getUserById(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Ucenik saveUser(Ucenik user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteUser(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
