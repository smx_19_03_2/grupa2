<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
    <head>
        <title>Strana za direktora</title>
        <link href="/webjars/bootstrap/4.3.0/css/bootstrap.min.css"
              rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/style.css">
    </head>    
    <body>
        <div id="wrapper">
            <ul class="tabs">
                <li><a href="#" name="tab6">Statistika uspesnosti po odeljenjima</a></li>
                <li><a href="#" name="tab7">Statistika uspesnosti po predmetima</a></li>
            </ul>
            <div id="content">
                <div id="tab6">
                    <div id="biranjeOdeljenja">
                        <h3>Izaberi odeljenje</h3>
                        <select class="izborOdeljenja">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
                <div id="tab7">
                    <div id="biranjePredmeta">
                        <h2>Izaberi predmet</h2>
                        <select class="izborPredmeta">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
            </div>
            <script src="/webjars/jquery/3.2.0/jquery.min.js"></script>
            <script src="/webjars/bootstrap/4.3.0/js/bootstrap.min.js"></script>
            <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
            <script src="/design.js"></script>
        </div>
    </body>
</html>