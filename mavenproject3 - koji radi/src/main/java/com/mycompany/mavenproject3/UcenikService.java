/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3;

import com.mycompany.mavenproject3.model.GodinaTest;
import com.mycompany.mavenproject3.model.UcenikTest;

/**
 *
 * @author Grupa1
 */
public interface UcenikService {
     public Iterable<UcenikTest> listAllUsers();

	   public UcenikTest getUserById(long id);

	   public UcenikTest saveUser(UcenikTest user);
	    
	   public void deleteUser(long id);
           
           public UcenikTest promeniUcenika (UcenikTest u);
           
           public Iterable <GodinaTest> listAllGodina();
           
         
}
