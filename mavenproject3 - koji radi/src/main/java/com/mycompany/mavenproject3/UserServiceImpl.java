package com.mycompany.mavenproject3;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.mavenproject3.model.Korisnik;
import com.mycompany.mavenproject3.FunkcijaRepository;
import com.mycompany.mavenproject3.KorisnikRepository;
import com.mycompany.mavenproject3.UserService;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private KorisnikRepository korisnikRepo;
    @Autowired
    private FunkcijaRepository funkcijaRepository;
 

//    @Override
//    public void save(Korisnik korisnik) {
//        korisnik.setLozinka(korisnik.getLozinka());
//        //korisnik.setRoles(new HashSet<>(roleRepository.findAll()));
//        
//        
//        korisnik.setFunkcija(funkcijaRepository.findAll().get(0));
//
//    }

	
	@Override
	public Long findByEmailAndPassword( String email, String lozinka) {
	Long funkcija_id = korisnikRepo.findFunkcija(email, lozinka);
		return funkcija_id;
	}

    

    @Override
    public Korisnik findKorisnikByEmailAndPassword(String email, String lozinka) {
Korisnik k = korisnikRepo.findKorisnik(email, lozinka);
return k;
    }

    
}
