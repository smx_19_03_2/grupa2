/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3;

import com.mycompany.mavenproject3.model.Korisnik;
import com.mycompany.mavenproject3.KorisnikRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 *
 * @author Grupa1
 */
@Controller
public class LoginController {

    @Autowired
    private KorisnikRepository korisnikRepository;

    @PostMapping(path = "/login")

    @ResponseBody
    ModelAndView login(@RequestParam String email, @RequestParam String lozinka) {

        try {

            Korisnik k = korisnikRepository.findKorisnik(email, lozinka);

            System.out.println("KOrisnik je " + k.toString());
            Long funkcija_id = k.getFunkcija().getId();

            if (funkcija_id == 1L) {
                return new ModelAndView("redirect:/admin");
            } else if (funkcija_id == 2L) {
                return new ModelAndView("redirect:/direktor");
            } else if (funkcija_id == 3L) {
                System.out.println("funkcija id iz ucitelja je " + funkcija_id);
                return new ModelAndView("redirect:/ucitelj");
            } else {
                return new ModelAndView("redirect:/roditelj");
            }

        } catch (Exception e) {
            return new ModelAndView("redirect:/index");

        }
    }
}
