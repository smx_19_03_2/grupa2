/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3;

import com.mycompany.mavenproject3.model.UcenikTest;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Grupa1
 */
public interface UcenikRepository extends CrudRepository<UcenikTest, Long> {

}
