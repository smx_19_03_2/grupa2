/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

 @Entity
 @Table(name="UCENIK")
public class UcenikTest {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    
    @ManyToOne 
    @JoinColumn(name="godina_id")
    private GodinaTest godina;

    private String Ime;

    private String Prezime;

    public UcenikTest() {
    }

    public UcenikTest(String Ime, String Prezime) {
        this.Ime = Ime;
        this.Prezime = Prezime;
    }

    public UcenikTest(GodinaTest godina, String Ime, String Prezime) {
        this.godina = godina;
        this.Ime = Ime;
        this.Prezime = Prezime;
    }

   

    
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIme() {
        return Ime;
    }

    public void setIme(String Ime) {
        this.Ime = Ime;
    }

    public String getPrezime() {
        return Prezime;
    }

    public void setPrezime(String Prezime) {
        this.Prezime = Prezime;
    }

    public GodinaTest getGodina() {
        return godina;
    }

    public void setGodina(GodinaTest godina) {
        this.godina = godina;
    }

    @Override
    public String toString() {
        return "Ucenik{" + "id=" + id + ", godina=" + godina + ", Ime=" + Ime + ", Prezime=" + Prezime + '}';
    }

   
   

   

    
    

    
    
    
}
