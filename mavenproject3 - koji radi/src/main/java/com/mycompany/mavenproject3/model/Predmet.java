/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Grupa1
 */

@Entity
@Table(name = "PREDMET")
public class Predmet {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String naziv;

    @ManyToMany (mappedBy="predmeti")
    private Set <Ucenik> ucenici = new HashSet<>();
    
    @ManyToOne
   @JoinTable(name="RASPOREDCASOVA",
           inverseJoinColumns= @JoinColumn(name="predmet_id",referencedColumnName = "ID"))
    private RasporedCasova rasporedCasova;
    

    
    public Predmet() {
    }

    public Predmet(String naziv) {
        this.naziv = naziv;
    }

    public Predmet(long id, String naziv) {
        this.id = id;
        this.naziv = naziv;
    }

    public Predmet(String naziv, RasporedCasova rasporedCasova) {
        this.naziv = naziv;
        this.rasporedCasova = rasporedCasova;
    }
    

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Set<Ucenik> getUcenici() {
        return ucenici;
    }

    public void setUcenici(Set<Ucenik> ucenici) {
        this.ucenici = ucenici;
    }

    public RasporedCasova getRasporedCasova() {
        return rasporedCasova;
    }

    public void setRasporedCasova(RasporedCasova rasporedCasova) {
        this.rasporedCasova = rasporedCasova;
    }

    @Override
    public String toString() {
        return "Predmet{" + "id=" + id + ", naziv=" + naziv + ", ucenici=" + ucenici + ", rasporedCasova=" + rasporedCasova + '}';
    }
    

    
    
    

   
    
    
}
