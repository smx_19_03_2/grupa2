/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Grupa1
 */
@Entity
@Table(name="KORISNIK")
public class Korisnik {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private String ime, prezime, lozinka, email;
    
     @ManyToOne 
    @JoinColumn(name="funkcija_id")
    private Funkcija funkcija;
     
     

    public Korisnik() {
    }

    public Korisnik(String ime, String prezime, String lozinka, String email, Funkcija funkcija) {
        this.ime = ime;
        this.prezime = prezime;
        this.lozinka = lozinka;
        this.email = email;
        this.funkcija = funkcija;
    }

    public Korisnik(long id, String ime, String prezime, String lozinka, String email, Funkcija funkcija) {
        this.id = id;
        this.ime = ime;
        this.prezime = prezime;
        this.lozinka = lozinka;
        this.email = email;
        this.funkcija = funkcija;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Funkcija getFunkcija() {
        return funkcija;
    }

    public void setFunkcija(Funkcija funkcija) {
        this.funkcija = funkcija;
    }

    @Override
    public String toString() {
        return "Korisnik{" + "id=" + id + ", ime=" + ime + ", prezime=" + prezime + ", lozinka=" + lozinka + ", email=" + email + ", funkcija=" + funkcija + '}';
    }
    
    
}
