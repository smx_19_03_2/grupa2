/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.model;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Grupa1
 */

 @Entity 
 @Table(name = "GODINA")
public class GodinaTest {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private String godina;
   
//    @OneToMany(mappedBy = "ucenik")
//    private List<Ucenik> ucenici;

    public GodinaTest() {
    }

    public GodinaTest(long id) {
        this.id = id;
    }

    
    public GodinaTest(String godina) {
        this.godina = godina;
    }

    public GodinaTest(long id, String godina) {
        this.id = id;
        this.godina = godina;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGodina() {
        return godina;
    }

    public void setGodina(String godina) {
        this.godina = godina;
    }

//    public List<Ucenik> getUcenici() {
//        return ucenici;
//    }
//
//    public void setUcenici(List<Ucenik> ucenici) {
//        this.ucenici = ucenici;
//    }
//
//    @Override
//    public String toString() {
//        return "Godina{" + "id=" + id + ", godina=" + godina + ", ucenici=" + ucenici + '}';
//    }

    @Override
    public String toString() {
        return "Godina{" + "id=" + id + ", godina=" + godina + '}';
    }

    
    

  
    
    
    
    
}
