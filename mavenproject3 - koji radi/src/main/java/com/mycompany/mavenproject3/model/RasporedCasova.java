/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Grupa1
 */
@Entity
@Table(name="RasporedCasova")
public class RasporedCasova {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    private Date datum;
    
    @OneToMany
    @JoinColumn(name="predmet_id")
    private Set <Predmet> predmeti = new HashSet<>();

    public RasporedCasova() {
    }

    public RasporedCasova(long id, Date datum) {
        this.id = id;
        this.datum = datum;
    }

   

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

//    public Set<Predmet> getPredmeti() {
//        return predmeti;
//    }
//
//    public void setPredmeti(Set<Predmet> predmeti) {
//        this.predmeti = predmeti;
//    }
//

    @Override
    public String toString() {
        return "RasporedCasova{" + "id=" + id + ", datum=" + datum + '}';
    }


   
    
}
