/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Grupa1
 */
@Entity
@Table(name = "OCENA")
public class Ocena {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    private String ocena;

    public Ocena() {
    }

    public Ocena(String ocena) {
        this.ocena = ocena;
    }

    public Ocena(long id, String ocena) {
        this.id = id;
        this.ocena = ocena;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOcena() {
        return ocena;
    }

    public void setOcena(String ocena) {
        this.ocena = ocena;
    }

    @Override
    public String toString() {
        return "Ocena{" + "id=" + id + ", ocena=" + ocena + '}';
    }
    
    
}
