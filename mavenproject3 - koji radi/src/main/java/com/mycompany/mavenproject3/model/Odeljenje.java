/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Grupa1
 */
@Entity
@Table(name="ODELJENJE")
public class Odeljenje {
     @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private String naziv;
    
    @OneToOne
    @JoinColumn(name = "raspored_casova_id")
    private RasporedCasova rasporedCasova;
    
   

    public Odeljenje() {
    }

    public Odeljenje(String naziv) {
        this.naziv = naziv;
    }

    public Odeljenje(long id, String naziv) {
        this.id = id;
        this.naziv = naziv;
    }

    public Odeljenje(String naziv, RasporedCasova rasporedCasova) {
        this.naziv = naziv;
        this.rasporedCasova = rasporedCasova;
    }
    
    

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public RasporedCasova getRasporedCasova() {
        return rasporedCasova;
    }

    public void setRasporedCasova(RasporedCasova rasporedCasova) {
        this.rasporedCasova = rasporedCasova;
    }

    @Override
    public String toString() {
        return "Odeljenje{" + "id=" + id + ", naziv=" + naziv + ", rasporedCasova=" + rasporedCasova + '}';
    }
    

    
    
    
}
