/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Grupa1
 */
@Entity
@Table(name = "RODITELJ")
public class Roditelj {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToOne
    @JoinColumn(name = "korisnik_id")
    private Korisnik korisnik;

    @ManyToMany
    @JoinTable(
            name = "ROD_UCE",
            joinColumns
            = @JoinColumn(name = "RODITELJ_ID", referencedColumnName = "ID"),
            inverseJoinColumns
            = @JoinColumn(name = "UCENIK_ID", referencedColumnName = "ID"))
    private Set <Ucenik> ucenici = new HashSet<>();

    public Roditelj() {
    }

    public Roditelj(long id) {
        this.id = id;
    }

    public Roditelj(long id, Korisnik korisnik) {
        this.id = id;
        this.korisnik = korisnik;
    }


    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public Set<Ucenik> getUcenici() {
        return ucenici;
    }

    public void setUcenici(Set<Ucenik> ucenici) {
        this.ucenici = ucenici;
    }

    @Override
    public String toString() {
        return "Roditelj{" + "id=" + id + ", korisnik=" + korisnik + ", ucenici=" + ucenici + '}';
    }

   
   

}
