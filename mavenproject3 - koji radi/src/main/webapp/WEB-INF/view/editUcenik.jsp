<%-- 
    Document   : editUcenik
    Created on : Mar 19, 2019, 9:56:02 AM
    Author     : Grupa1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>JSP Page</title>
        </head>
        <body>
            <h1><h:outputText value="Hello World!"/></h1>
            <div class="container">
                <div>

                    <table>
                        <tr></tr>
                        <td><b>Id</b></td>
                        <td>${ucenik.id}</td>
                        <tr>
                            <td><b>Ime</b></td>
                            <td>"${ucenik.ime}"</td>
                        </tr>
                        <tr>
                            <td><b>Prezime</b></td>
                            <td>"${ucenik.prezime}"</td>
                        </tr>
                        <tr>
                            <td><b>Godina</b></td>
                            <td>${ucenik.godina}</td>
                        </tr>
                    </table>
                </div>
                <form action='/ucenik/update' method='post'>



                    <table class='table table-hover table-responsive table-bordered'>

                        <tr>
                            <td><b>Ime</b></td>
                            <td><input type='text' name='ime' class='form-control' value="${ucenik.ime}" /></td>
                        </tr>

                        <tr>
                            <td><b>Prezime</b></td>
                            <td><input type='text' name='prezime' class='form-control' value="${ucenik.prezime}" /></td>
                        </tr>

                        <label>Godina: </label> 

                        <select name="godinaId">
                            <c:forEach items="${listGodina}" var="lg">
                                <option  value="<c:out value="${lg.id}"></c:out>">${lg.godina}</option>
                            </c:forEach>
                        </select>

                        <input type="hidden" id='id' rows='5' class='form-control' name='id' value="${ucenik.id}" />
                        <tr>
                            <td></td>
                            <td>
                                <button type="submit" class="btn btn-primary">Update Ucenik Information</button>
                            </td>
                        </tr>

                    </table>
                </form>


            </div>
        </body>
    </html>
</f:view>
