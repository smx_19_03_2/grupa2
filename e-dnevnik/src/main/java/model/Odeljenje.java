/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Grupa1
 */
public class Odeljenje {
    
    private int id;
    private String naziv;
    private int ucitelj_id;

    public Odeljenje() {
    }
    
    public Odeljenje(String naziv, int ucitelj_id) {

        this.naziv = naziv;
        this.ucitelj_id = ucitelj_id;
    }

    public Odeljenje(int id, String naziv, int ucitelj_id) {
        this.id = id;
        this.naziv = naziv;
        this.ucitelj_id = ucitelj_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public int getUcitelj_id() {
        return ucitelj_id;
    }

    public void setUcitelj_id(int ucitelj_id) {
        this.ucitelj_id = ucitelj_id;
    }

    @Override
    public String toString() {
        return "Odeljenje{" + "id=" + id + ", naziv=" + naziv + ", ucitelj_id=" + ucitelj_id + '}';
    }
    
    
    
    
}
