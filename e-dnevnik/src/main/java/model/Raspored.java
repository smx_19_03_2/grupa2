/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Grupa1
 */
public class Raspored {
    
    private int id;
    private String dan;
    private int broj_casa;
    private int predmet_id;

    public Raspored() {
    }
    
    
    public Raspored( String dan, int broj_casa, int predmet_id) {
        this.dan = dan;
        this.broj_casa = broj_casa;
        this.predmet_id = predmet_id;
    }

    public Raspored(int id, String dan, int broj_casa, int predmet_id) {
        this.id = id;
        this.dan = dan;
        this.broj_casa = broj_casa;
        this.predmet_id = predmet_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDan() {
        return dan;
    }

    public void setDan(String dan) {
        this.dan = dan;
    }

    public int getBroj_casa() {
        return broj_casa;
    }

    public void setBroj_casa(int broj_casa) {
        this.broj_casa = broj_casa;
    }

    public int getPredmet_id() {
        return predmet_id;
    }

    public void setPredmet_id(int predmet_id) {
        this.predmet_id = predmet_id;
    }

    @Override
    public String toString() {
        return "Raspored{" + "id=" + id + ", dan=" + dan + ", broj_casa=" + broj_casa + ", predmet_id=" + predmet_id + '}';
    }
    
    
    
}
