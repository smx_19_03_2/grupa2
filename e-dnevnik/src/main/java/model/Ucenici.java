/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Grupa1
 */
public class Ucenici {
    
    private int id;
    private String ime;
    private String prezime;
    private String odeljenje_id;    

    public Ucenici() {
    }
    
   
    public Ucenici( String ime, String prezime, String odeljenje_id) {
    
        this.ime = ime;
        this.prezime = prezime;
        this.odeljenje_id = odeljenje_id;
    }

    public Ucenici(int id, String ime, String prezime, String odeljenje_id) {
        this.id = id;
        this.ime = ime;
        this.prezime = prezime;
        this.odeljenje_id = odeljenje_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getOdeljenje_id() {
        return odeljenje_id;
    }

    public void setOdeljenje_id(String odeljenje_id) {
        this.odeljenje_id = odeljenje_id;
    }

    @Override
    public String toString() {
        return "Ucenici{" + "id=" + id + ", ime=" + ime + ", prezime=" + prezime + ", odeljenje_id=" + odeljenje_id + '}';
    }
    
    
    
}

