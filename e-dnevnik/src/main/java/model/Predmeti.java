/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Grupa1
 */
public class Predmeti {
    
    private int id;
    private String obavezni;
    private String izborni;

    public Predmeti() {
    }
    

    public Predmeti(int id, String obavezni, String izborni) {
        this.id = id;
        this.obavezni = obavezni;
        this.izborni = izborni;
    }
    
    
    public Predmeti( String obavezni, String izborni) {
        this.obavezni = obavezni;
        this.izborni = izborni;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getObavezni() {
        return obavezni;
    }

    public void setObavezni(String obavezni) {
        this.obavezni = obavezni;
    }

    public String getIzborni() {
        return izborni;
    }

    public void setIzborni(String izborni) {
        this.izborni = izborni;
    }

    @Override
    public String toString() {
        return "Predmeti{" + "id=" + id + ", obavezni=" + obavezni + ", izborni=" + izborni + '}';
    }
    
    
    
}
