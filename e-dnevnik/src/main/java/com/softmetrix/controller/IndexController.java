package com.softmetrix.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexController {

	@RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
	public String index(Model model) {
		
		return "index";
	}
        @RequestMapping(value = { "/", "/admin" }, method = RequestMethod.GET)
	public String admin(Model model) {
		
		return "admin";
	}
        @RequestMapping(value = { "/", "/direktor" }, method = RequestMethod.GET)
	public String direktor(Model model) {
		
		return "direktor";
	}
        @RequestMapping(value = { "/", "/roditelj" }, method = RequestMethod.GET)
	public String roditelj(Model model) {
		
		return "roditelj";
	}
        @RequestMapping(value = { "/", "/ucitelj" }, method = RequestMethod.GET)
	public String ucitelj(Model model) {
		
		return "ucitelj";
	}
        @RequestMapping(value = { "/", "/komunikacija" }, method = RequestMethod.GET)
	public String komunikacija(Model model) {
		
		return "komunikacija";
	}
        
}
  	