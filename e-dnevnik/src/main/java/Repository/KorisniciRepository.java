/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repository;

import java.io.Serializable;
import model.Korisnici;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Grupa1
 */
public interface KorisniciRepository extends CrudRepository<Korisnici, Serializable>{
    
}
