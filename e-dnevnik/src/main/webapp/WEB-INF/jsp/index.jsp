<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
    <head>
        <title>Welcome</title>
        <link href="/webjars/bootstrap/4.3.0/css/bootstrap.min.css"
              rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/style.css">

    </head>
    <body>
        <div id="wrapper">
            <h1 style="padding: 20px">eDnevnik</h1>
            <div class="container" style="margin-top: 100px">

                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address:</label> <input
                            type="email" class="form-control" id="exampleInputEmail1"
                            aria-describedby="emailHelp" placeholder="Enter email"> <small
                            id="emailHelp" class="form-text text-muted">We'll never
                            share your email with anyone else.</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password:</label> <input
                            type="password" class="form-control" id="exampleInputPassword1"
                            placeholder="Password">
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Check me
                            out</label>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>


            </div>
            <script src="/webjars/jquery/3.2.0/jquery.min.js"></script>
            <script src="/webjars/bootstrap/4.3.0/js/bootstrap.min.js"></script>
            <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
            <script src="/design.js"></script>
        </div>

    </body>
</html>
