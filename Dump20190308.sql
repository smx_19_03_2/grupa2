-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: e_dnevnik_ana
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `idAdmin` int(11) NOT NULL AUTO_INCREMENT,
  `Korisnik_Korisnik_id` int(11) NOT NULL,
  PRIMARY KEY (`idAdmin`) USING BTREE,
  KEY `fk_admin_Korisnik1_idx` (`Korisnik_Korisnik_id`),
  CONSTRAINT `fk_admin_Korisnik1` FOREIGN KEY (`Korisnik_Korisnik_id`) REFERENCES `korisnik` (`Korisnik_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `direktor`
--

DROP TABLE IF EXISTS `direktor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `direktor` (
  `idDirektor` int(11) NOT NULL AUTO_INCREMENT,
  `Korisnik_Korisnik_id` int(11) NOT NULL,
  PRIMARY KEY (`idDirektor`) USING BTREE,
  KEY `fk_direktor_Korisnik1_idx` (`Korisnik_Korisnik_id`),
  CONSTRAINT `fk_direktor_Korisnik1` FOREIGN KEY (`Korisnik_Korisnik_id`) REFERENCES `korisnik` (`Korisnik_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `direktor`
--

LOCK TABLES `direktor` WRITE;
/*!40000 ALTER TABLE `direktor` DISABLE KEYS */;
/*!40000 ALTER TABLE `direktor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funkcija`
--

DROP TABLE IF EXISTS `funkcija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funkcija` (
  `Uloga_id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv_uloge` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Uloga_id`),
  UNIQUE KEY `naziv_uloge_UNIQUE` (`naziv_uloge`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funkcija`
--

LOCK TABLES `funkcija` WRITE;
/*!40000 ALTER TABLE `funkcija` DISABLE KEYS */;
INSERT INTO `funkcija` VALUES (4,'admin'),(1,'direktor'),(3,'roditelj'),(2,'ucitelj');
/*!40000 ALTER TABLE `funkcija` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `korisnik`
--

DROP TABLE IF EXISTS `korisnik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `korisnik` (
  `Korisnik_id` int(11) NOT NULL AUTO_INCREMENT,
  `Ime` varchar(45) DEFAULT NULL,
  `Prezime` varchar(45) DEFAULT NULL,
  `lozinka` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `Funkcija_Uloga_id` int(11) NOT NULL,
  PRIMARY KEY (`Korisnik_id`,`Funkcija_Uloga_id`),
  KEY `fk_Korisnik_Funkcija1_idx` (`Funkcija_Uloga_id`),
  CONSTRAINT `fk_Korisnik_Funkcija1` FOREIGN KEY (`Funkcija_Uloga_id`) REFERENCES `funkcija` (`Uloga_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `korisnik`
--

LOCK TABLES `korisnik` WRITE;
/*!40000 ALTER TABLE `korisnik` DISABLE KEYS */;
INSERT INTO `korisnik` VALUES (2,'Pera','Peric','pass','mail@gmail.com',2);
/*!40000 ALTER TABLE `korisnik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocena`
--

DROP TABLE IF EXISTS `ocena`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ocena` (
  `Ucenik_id` int(11) NOT NULL,
  `Predmet_id` int(11) NOT NULL,
  `ocena` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Ucenik_id`,`Predmet_id`) USING BTREE,
  KEY `fk_Ucenik_has_Predmet_Predmet1_idx` (`Predmet_id`) USING BTREE,
  KEY `fk_Ucenik_has_Predmet_Ucenik1_idx` (`Ucenik_id`) USING BTREE,
  CONSTRAINT `fk_Ucenik_has_Predmet_Predmet1` FOREIGN KEY (`Predmet_id`) REFERENCES `predmet` (`idPredmet`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Ucenik_has_Predmet_Ucenik1` FOREIGN KEY (`Ucenik_id`) REFERENCES `ucenik` (`idUcenik`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocena`
--

LOCK TABLES `ocena` WRITE;
/*!40000 ALTER TABLE `ocena` DISABLE KEYS */;
/*!40000 ALTER TABLE `ocena` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `odeljenje`
--

DROP TABLE IF EXISTS `odeljenje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `odeljenje` (
  `idOdeljenje` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idOdeljenje`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `odeljenje`
--

LOCK TABLES `odeljenje` WRITE;
/*!40000 ALTER TABLE `odeljenje` DISABLE KEYS */;
INSERT INTO `odeljenje` VALUES (1,'I/1'),(2,'I/2'),(3,'I/3'),(4,'II/1'),(5,'II/2'),(6,'II/3');
/*!40000 ALTER TABLE `odeljenje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `otvorena_vrata`
--

DROP TABLE IF EXISTS `otvorena_vrata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `otvorena_vrata` (
  `idOtvorena vrata` int(11) NOT NULL AUTO_INCREMENT,
  `Datum` date DEFAULT NULL,
  `Vreme` varchar(45) DEFAULT NULL,
  `Roditelj_idRoditelj` int(11) NOT NULL,
  `Ucitelj_idUcitelj` int(11) NOT NULL,
  PRIMARY KEY (`idOtvorena vrata`) USING BTREE,
  KEY `fk_Otvorena vrata_Roditelj1_idx` (`Roditelj_idRoditelj`) USING BTREE,
  KEY `fk_Otvorena vrata_Ucitelj1_idx` (`Ucitelj_idUcitelj`) USING BTREE,
  CONSTRAINT `fk_Otvorena vrata_Roditelj1` FOREIGN KEY (`Roditelj_idRoditelj`) REFERENCES `roditelj` (`idRoditelj`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Otvorena vrata_Ucitelj1` FOREIGN KEY (`Ucitelj_idUcitelj`) REFERENCES `ucitelj` (`idUcitelj`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `otvorena_vrata`
--

LOCK TABLES `otvorena_vrata` WRITE;
/*!40000 ALTER TABLE `otvorena_vrata` DISABLE KEYS */;
/*!40000 ALTER TABLE `otvorena_vrata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `predmet`
--

DROP TABLE IF EXISTS `predmet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `predmet` (
  `idPredmet` int(11) NOT NULL AUTO_INCREMENT,
  `Naziv` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idPredmet`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `predmet`
--

LOCK TABLES `predmet` WRITE;
/*!40000 ALTER TABLE `predmet` DISABLE KEYS */;
INSERT INTO `predmet` VALUES (1,'matematika'),(2,'srpski'),(3,'likovno'),(4,'fizicko');
/*!40000 ALTER TABLE `predmet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `primer`
--

DROP TABLE IF EXISTS `primer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `primer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(45) DEFAULT NULL,
  `prezime` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `primer`
--

LOCK TABLES `primer` WRITE;
/*!40000 ALTER TABLE `primer` DISABLE KEYS */;
INSERT INTO `primer` VALUES (1,'pera','peric');
/*!40000 ALTER TABLE `primer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `raspored_casova`
--

DROP TABLE IF EXISTS `raspored_casova`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raspored_casova` (
  `Predmet_id` int(11) NOT NULL,
  `Odeljenje_id` int(11) NOT NULL,
  `Dan` varchar(45) DEFAULT NULL,
  `Vreme` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Predmet_id`,`Odeljenje_id`) USING BTREE,
  KEY `fk_Predmet_has_Odeljenje_Odeljenje1_idx` (`Odeljenje_id`) USING BTREE,
  KEY `fk_Predmet_has_Odeljenje_Predmet1_idx` (`Predmet_id`) USING BTREE,
  CONSTRAINT `fk_Predmet_has_Odeljenje_Odeljenje1` FOREIGN KEY (`Odeljenje_id`) REFERENCES `odeljenje` (`idOdeljenje`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Predmet_has_Odeljenje_Predmet1` FOREIGN KEY (`Predmet_id`) REFERENCES `predmet` (`idPredmet`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `raspored_casova`
--

LOCK TABLES `raspored_casova` WRITE;
/*!40000 ALTER TABLE `raspored_casova` DISABLE KEYS */;
/*!40000 ALTER TABLE `raspored_casova` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roditelj`
--

DROP TABLE IF EXISTS `roditelj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roditelj` (
  `idRoditelj` int(11) NOT NULL AUTO_INCREMENT,
  `poruke` longtext,
  `Korisnik_Korisnik_id` int(11) NOT NULL,
  PRIMARY KEY (`idRoditelj`) USING BTREE,
  KEY `fk_roditelj_Korisnik1_idx` (`Korisnik_Korisnik_id`),
  CONSTRAINT `fk_roditelj_Korisnik1` FOREIGN KEY (`Korisnik_Korisnik_id`) REFERENCES `korisnik` (`Korisnik_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roditelj`
--

LOCK TABLES `roditelj` WRITE;
/*!40000 ALTER TABLE `roditelj` DISABLE KEYS */;
/*!40000 ALTER TABLE `roditelj` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roditelj_has_ucenik`
--

DROP TABLE IF EXISTS `roditelj_has_ucenik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roditelj_has_ucenik` (
  `Roditelj_idRoditelj` int(11) NOT NULL,
  `Ucenik_idUcenik` int(11) NOT NULL,
  PRIMARY KEY (`Roditelj_idRoditelj`,`Ucenik_idUcenik`) USING BTREE,
  KEY `fk_Roditelj_has_Ucenik_Ucenik1_idx` (`Ucenik_idUcenik`) USING BTREE,
  KEY `fk_Roditelj_has_Ucenik_Roditelj1_idx` (`Roditelj_idRoditelj`) USING BTREE,
  CONSTRAINT `fk_Roditelj_has_Ucenik_Roditelj1` FOREIGN KEY (`Roditelj_idRoditelj`) REFERENCES `roditelj` (`idRoditelj`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Roditelj_has_Ucenik_Ucenik1` FOREIGN KEY (`Ucenik_idUcenik`) REFERENCES `ucenik` (`idUcenik`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roditelj_has_ucenik`
--

LOCK TABLES `roditelj_has_ucenik` WRITE;
/*!40000 ALTER TABLE `roditelj_has_ucenik` DISABLE KEYS */;
/*!40000 ALTER TABLE `roditelj_has_ucenik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ucenik`
--

DROP TABLE IF EXISTS `ucenik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ucenik` (
  `idUcenik` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(45) DEFAULT NULL,
  `prezime` varchar(45) DEFAULT NULL,
  `Odeljenje_idOdeljenje` int(11) NOT NULL,
  PRIMARY KEY (`idUcenik`) USING BTREE,
  KEY `fk_Ucenik_Odeljenje1_idx` (`Odeljenje_idOdeljenje`) USING BTREE,
  CONSTRAINT `fk_Ucenik_Odeljenje1` FOREIGN KEY (`Odeljenje_idOdeljenje`) REFERENCES `odeljenje` (`idOdeljenje`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ucenik`
--

LOCK TABLES `ucenik` WRITE;
/*!40000 ALTER TABLE `ucenik` DISABLE KEYS */;
/*!40000 ALTER TABLE `ucenik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ucitelj`
--

DROP TABLE IF EXISTS `ucitelj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ucitelj` (
  `idUcitelj` int(11) NOT NULL AUTO_INCREMENT,
  `Odeljenje_idOdeljenje` int(11) NOT NULL,
  `poruke` longtext,
  `Korisnik_Korisnik_id` int(11) NOT NULL,
  PRIMARY KEY (`idUcitelj`,`Odeljenje_idOdeljenje`) USING BTREE,
  KEY `fk_Ucitelj_Odeljenje1_idx` (`Odeljenje_idOdeljenje`) USING BTREE,
  KEY `fk_ucitelj_Korisnik1_idx` (`Korisnik_Korisnik_id`),
  CONSTRAINT `fk_Ucitelj_Odeljenje1` FOREIGN KEY (`Odeljenje_idOdeljenje`) REFERENCES `odeljenje` (`idOdeljenje`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ucitelj_Korisnik1` FOREIGN KEY (`Korisnik_Korisnik_id`) REFERENCES `korisnik` (`Korisnik_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ucitelj`
--

LOCK TABLES `ucitelj` WRITE;
/*!40000 ALTER TABLE `ucitelj` DISABLE KEYS */;
/*!40000 ALTER TABLE `ucitelj` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'e_dnevnik_ana'
--

--
-- Dumping routines for database 'e_dnevnik_ana'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-08 11:27:38
