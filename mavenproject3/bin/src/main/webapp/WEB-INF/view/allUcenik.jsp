<%-- 
    Document   : SviUcenici
    Created on : Mar 15, 2019, 11:53:59 AM
    Author     : Grupa1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>JSP Page</title>
        </head>
        <body>
            <h1><h:outputText value="Hello World!"/></h1>


            <table class="table table-hover">

                <thead>
                    <tr>
                        <th><b>Ime</b></th>
                        <th><b>Prezime</b></th>

                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${list}" var="lou">
                        <tr>

                            <td><c:out value="${lou.ime}"></c:out></td>
                            <td><c:out value="${lou.prezime}"></c:out></td>
                            <td><c:out value="${lou.godina}"></c:out></td>

                                <td>
                                    <a href="/ucenik/${lou.id}/editUcenik">
                                    <button type="submit" class="btn btn-primary">Edit User</button>
                                </a>
                            </td>
                            <td>
                                <a href="/ucenik/${lou.id}/delete">
                                    <button type="submit" class="btn btn-primary">Delete User</button>
                                </a>
                            </td>
                        </tr>

                    </c:forEach>
                </tbody>
            </table>
        </body>


    </html>
</f:view>
