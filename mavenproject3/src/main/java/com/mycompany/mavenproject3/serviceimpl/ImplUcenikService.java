/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.serviceimpl;

import com.mycompany.mavenproject3.Application;
import com.mycompany.mavenproject3.model.GodinaTest;
import com.mycompany.mavenproject3.model.UcenikTest;
import com.mycompany.mavenproject3.repository.GodinaRepository;
import com.mycompany.mavenproject3.repository.KorisnikRepository;
import com.mycompany.mavenproject3.repository.UcenikRepository;
import com.mycompany.mavenproject3.service.UcenikService;

import java.util.List;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 *
 * @author Grupa1
 */
@Service
public class ImplUcenikService implements UcenikService {

	private UcenikRepository ucenikRepo;

	@Autowired
	private GodinaRepository godinaRepo;
	@Autowired
	private KorisnikRepository korisnikRepo;

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	@Autowired
	public void setUcenikRepo(UcenikRepository ucenikRepo) {
		this.ucenikRepo = ucenikRepo;
	}

	@Autowired
	public void setGodinaReepo(GodinaRepository godinaRepo) {
		this.godinaRepo = godinaRepo;
		
		
	}

	@Autowired
	public void setKorisnikReepo(KorisnikRepository korisnikRepo) {
		this.korisnikRepo = korisnikRepo;
		
		
	}
	
	@Override
	public Iterable<UcenikTest> listAllUsers() {

		return ucenikRepo.findAll();

	}

	@Override
	public UcenikTest saveUser(UcenikTest user) {
		return ucenikRepo.save(user);
	}

	@Override
	public void deleteUser(long id) {
		ucenikRepo.deleteById(id);
	}

	@Override
	public UcenikTest getUserById(long id) {
		UcenikTest u = ucenikRepo.findById(id).get();
		return u;
		// return ucenikRepo.findById(id).orElseThrow(() -> new
		// EntityNotFoundException(id));
	}

	public UcenikTest promeniUcenika(UcenikTest u) {

		Optional<UcenikTest> ucenik = ucenikRepo.findById(u.getId());

		if (ucenik.isPresent()) {
			ucenik.get().setId(u.getId());
			ucenik.get().setIme(u.getIme());
			ucenik.get().setPrezime(u.getPrezime());
			return ucenikRepo.save(ucenik.get());
		} else
			return null;
	}



	public void FindAllGodinabyID(GodinaRepository godinaRepo, Long id) {
		List<GodinaTest> gtest = godinaRepo.FindAllGodinaTest(id);

		for (GodinaTest g : gtest)
			log.info("Godina je " + g);

	}

	public void findKorisnikFunkcijaById(KorisnikRepository korisnikRepo, String email, String lozinka) {
		Long funkcija_id = korisnikRepo.findFunkcija(email, lozinka);
		log.info("funkcija je: " + funkcija_id);

	}

	@Override
	public Iterable<GodinaTest> listAllGodina() {
		// TODO Auto-generated method stub
		return godinaRepo.findAll();
	}


	

}
