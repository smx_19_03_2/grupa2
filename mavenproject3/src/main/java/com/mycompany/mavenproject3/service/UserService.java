package com.mycompany.mavenproject3.service;

import com.mycompany.mavenproject3.model.Korisnik;

public interface UserService {
	
	void save(Korisnik korisnik);
	
	Korisnik findByEmail(String email);

}
