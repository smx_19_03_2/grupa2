package com.mycompany.mavenproject3.controller;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

//import hello.User;
//import hello.UserRepository;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;
import org.springframework.ui.Model;
//import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.mycompany.mavenproject3.Application;
import com.mycompany.mavenproject3.model.GodinaTest;
import com.mycompany.mavenproject3.repository.GodinaRepository;
import com.mycompany.mavenproject3.repository.KorisnikRepository;
import com.mycompany.mavenproject3.service.UcenikService;

@Controller
//@RequestMapping(path = {"/index" } )
public class MainController {

    @Autowired
    private GodinaRepository godinaRepository;

    @Autowired
    private UcenikService ucenikService;
    
    @Autowired
    private KorisnikRepository korisnikRepository;
    
    private static final Logger log = LoggerFactory.getLogger(Application.class);

    @GetMapping(path = {"/", "/index"})
    public ModelAndView index() {
        ModelAndView model = new ModelAndView("index");
        model.addObject("listGodina", ucenikService.listAllGodina());
        
        Long funkcija_id = korisnikRepository.findFunkcija("mail@gmail.com", "pass");
        System.out.println(funkcija_id + "funkcija");
        
        List <GodinaTest> godinaTestLista= godinaRepository.FindAllGodinaTest(2L);
        for(GodinaTest g : godinaTestLista) {
        System.out.println(g);}
        return model;
        
        

    }
}




//  @GetMapping(path = "/allUcenik")
//    public ModelAndView allUcenik() {
//        ModelAndView model = new ModelAndView("allUcenik");
//        model.addObject("list", ucenikService.listAllUsers());
//        return model;