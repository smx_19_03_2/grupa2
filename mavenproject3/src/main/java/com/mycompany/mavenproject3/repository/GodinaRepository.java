package com.mycompany.mavenproject3.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.mavenproject3.model.GodinaTest;

@Repository
public interface GodinaRepository extends JpaRepository<GodinaTest, Long> {
	
	@Query("select g from GodinaTest g where id=?1")
	public List<GodinaTest> FindAllGodinaTest(long id);

	

}
