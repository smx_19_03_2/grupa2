package com.mycompany.mavenproject3.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mycompany.mavenproject3.model.GodinaTest;
import com.mycompany.mavenproject3.model.Korisnik;

@Repository
public interface KorisnikRepository extends JpaRepository<Korisnik, Long> {
	

	@Query(" select k.funkcija.id from Korisnik k where k.email='mail@gmail.com' and k.lozinka='pass'")
	public Long findFunkcija(String email, String lozinka);

}
