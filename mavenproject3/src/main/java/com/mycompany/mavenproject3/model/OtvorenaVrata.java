/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Grupa1
 */
@Entity
@Table(name = "OTVORENAVRATA")
public class OtvorenaVrata {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "vreme")
    private Date vreme;

    @OneToOne
    @JoinColumn(name = "roditelj_id")
    private Roditelj roditelj;
    
    @OneToOne
    @JoinColumn(name = "ucitelj_id")
    private Ucitelj ucitelj;

    public OtvorenaVrata() {
    }

    public OtvorenaVrata(Date vreme, Roditelj roditelj, Ucitelj ucitelj) {
        this.vreme = vreme;
        this.roditelj = roditelj;
        this.ucitelj = ucitelj;
    }

    public OtvorenaVrata(long id, Date vreme, Roditelj roditelj, Ucitelj ucitelj) {
        this.id = id;
        this.vreme = vreme;
        this.roditelj = roditelj;
        this.ucitelj = ucitelj;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getVreme() {
        return vreme;
    }

    public void setVreme(Date vreme) {
        this.vreme = vreme;
    }

    public Roditelj getRoditelj() {
        return roditelj;
    }

    public void setRoditelj(Roditelj roditelj) {
        this.roditelj = roditelj;
    }

    public Ucitelj getUcitelj() {
        return ucitelj;
    }

    public void setUcitelj(Ucitelj ucitelj) {
        this.ucitelj = ucitelj;
    }

    @Override
    public String toString() {
        return "OtvorenaVrata{" + "id=" + id + ", vreme=" + vreme + ", roditelj=" + roditelj + ", ucitelj=" + ucitelj + '}';
    }

    
}
