/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Grupa1
 */
@Entity
@Table(name = "UCITELJ")
public class Ucitelj {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToOne
    @JoinColumn(name = "korisnik_id")
    private Korisnik korisnik;
    
     @OneToOne
    @JoinColumn(name = "odeljenje_id")
    private Odeljenje odeljenje;
    

    public Ucitelj() {
    }

    public Ucitelj(long id) {
        this.id = id;
    }

    public Ucitelj(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public Ucitelj(long id, Korisnik korisnik) {
        this.id = id;
        this.korisnik = korisnik;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    @Override
    public String toString() {
        return "Ucitelj{" + "id=" + id + ", korisnik=" + korisnik + '}';
    }

    
}
