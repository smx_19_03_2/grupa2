<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
    <head>
        <title>Strana za ucitelje</title>
        <link href="/webjars/bootstrap/4.3.0/css/bootstrap.min.css"
              rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/style.css">

    </head>
    <body>
        <div id="wrapper">

            <div>
                <ul id="tabs">
                    <li><a href="#" name="tab1">Odeljenje</a></li>
                    <li><a href="#" name="tab2">Ocene</a></li>
                    <li><a href="#" name="tab3">Raspored casova</a></li>
                    <li><a href="#" name="tab4">Zahtevi</a></li>
                    <li><a href="#" name="tab5">Poruke</a></li>
                    <li><a href="#" name="tab6">Obavestenja</a></li>    
                </ul>
            </div>

            <script src="/webjars/jquery/3.2.0/jquery.min.js"></script>
            <script src="/webjars/bootstrap/4.3.0/js/bootstrap.min.js"></script>
            <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
            <script src="/design.js"></script>

        </div>

    </body>
</html>