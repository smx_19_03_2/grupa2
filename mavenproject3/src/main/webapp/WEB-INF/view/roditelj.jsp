<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
    <head>
        <title>Strana za roditelje</title>
        <link href="/webjars/bootstrap/4.3.0/css/bootstrap.min.css"
              rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/style.css">

    </head>
    <body>
        <div id="wrapper">

            <div>
                <ul id="tabs">
                    <li><a href="#" name="tab1">Pogledaj ocene</a></li>
                    <li><a href="#" name="tab2">Pogledaj izostanke</a></li>
                    <li><a href="#" name="tab3">Zakazivanja</a></li>
                    <li><a href="#" name="tab4">Poruke</a></li>
                    <li><a href="#" name="tab5">Obavestenja</a></li>    
                </ul>
            </div>

            <div id="content"> 
                <div id="tab1">
                    <div class="container" id="dodavanjeKorisnika">
                        <form>
                            <div class="form-option">
                                <label for="exampleInputOption1">Izaberite ulogu:</label>
                                <select type="option" class="option-control" id="exampleInputOption1">
                                    <option>Administrator</option>
                                    <option>Direktor</option>
                                    <option>Nastavnik</option>
                                    <option>Roditelj</option>
                                    <option>Ucenik</option>
                                </select>
                                <br><br>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Enter First Name:</label> <input
                                    type="email" class="form-control" id="ime"
                                    aria-describedby="emailHelp" placeholder="New First Name">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Enter Last Name:</label> <input
                                    type="email" class="form-control" id="prezime"
                                    aria-describedby="emailHelp" placeholder="New Last Name">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Enter new Email address:</label> <input
                                    type="email" class="form-control" id="exampleInputEmail1"
                                    aria-describedby="emailHelp" placeholder="New email">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Enter new Password:</label> <input
                                    type="password" class="form-control" id="exampleInputPassword1"
                                    placeholder="New Password">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Repeat Password:</label> <input
                                    type="password" class="form-control" id="exampleInputPassword1"
                                    placeholder="Repeat New Password">
                            </div>
                            <br>
                            <hr>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>

            <script src="/webjars/jquery/3.2.0/jquery.min.js"></script>
            <script src="/webjars/bootstrap/4.3.0/js/bootstrap.min.js"></script>
            <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
            <script src="/design.js"></script>

        </div>
    </body>
</html>