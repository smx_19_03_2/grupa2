package hello;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity // This tells Hibernate to make a table out of this class
public class Roditelj {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    private String Ime;

    private String Prezime;
    
    private String Email;

    public Roditelj() {
    }

    public Roditelj(Integer id, String Ime, String Prezime, String Email) {
        this.id = id;
        this.Ime = Ime;
        this.Prezime = Prezime;
        this.Email = Email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIme() {
        return Ime;
    }

    public void setIme(String Ime) {
        this.Ime = Ime;
    }

    public String getPrezime() {
        return Prezime;
    }

    public void setPrezime(String Prezime) {
        this.Prezime = Prezime;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", Ime=" + Ime + ", Prezime=" + Prezime + ", Email=" + Email + '}';
    }
    
    

	
    
    
}

