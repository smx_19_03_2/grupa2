package hello;

import org.springframework.data.repository.CrudRepository;

import hello.Roditelj;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface RoditeljRepository extends CrudRepository<Roditelj, Integer> {

}
