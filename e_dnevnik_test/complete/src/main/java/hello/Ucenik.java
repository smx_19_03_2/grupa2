/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hello;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

 @Entity // This tells Hibernate to make a table out of this class
public class Ucenik {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    private String Ime;

    private String Prezime;

    public Ucenik() {
    }

    public Ucenik(Integer id, String Ime, String Prezime) {
        this.id = id;
        this.Ime = Ime;
        this.Prezime = Prezime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIme() {
        return Ime;
    }

    public void setIme(String Ime) {
        this.Ime = Ime;
    }

    public String getPrezime() {
        return Prezime;
    }

    public void setPrezime(String Prezime) {
        this.Prezime = Prezime;
    }

    @Override
    public String toString() {
        return "Ucenik{" + "id=" + id + ", Ime=" + Ime + ", Prezime=" + Prezime + '}';
    }

    
    
    
}
