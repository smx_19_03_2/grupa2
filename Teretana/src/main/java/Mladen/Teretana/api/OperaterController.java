package Mladen.Teretana.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import Mladen.Teretana.domain.dto.OperaterDto;
import Mladen.Teretana.domain.dto.request.LoginRequestDto;
import Mladen.Teretana.domain.dto.request.OperaterRequestDto;
import Mladen.Teretana.domain.dto.response.LoginResponseDto;
import Mladen.Teretana.domain.dto.response.RegistrujOperateraResponseDto;
import Mladen.Teretana.service.OperaterService;

@RestController
@RequestMapping("/operateri")
public class OperaterController
{
	@Autowired
	private OperaterService servis;
	
	@PostMapping("/signup")
	public @ResponseBody RegistrujOperateraResponseDto registrujOperatera(@RequestBody OperaterRequestDto operaterDto)
	{
		return servis.registrujOperateraS(operaterDto);
	}
	
	@PostMapping("/login")
	public @ResponseBody LoginResponseDto login(@RequestBody LoginRequestDto loginDto)
	{
		return servis.loginS(loginDto);
	}
	
	@GetMapping("/all")
	public @ResponseBody List<OperaterDto> listajOperatere()
	{
		return servis.listajOperatereS();
	}
}