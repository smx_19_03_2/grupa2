package Mladen.Teretana.api;

import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import Mladen.Teretana.domain.Dolazak;
import Mladen.Teretana.domain.dto.KorisnikDto;
import Mladen.Teretana.domain.dto.response.DodajDolazakResponseDto;
import Mladen.Teretana.domain.dto.response.DodajOdlazakResponseDto;
import Mladen.Teretana.domain.dto.response.DodajTreningResponseDto;
import Mladen.Teretana.service.DolazakService;

@RestController
@RequestMapping("/dolasci")
public class DolazakController 
{
	

	@Autowired
	private DolazakService servis;
	
	@PostMapping("/add/{idk}")
	public @ResponseBody DodajDolazakResponseDto dodajDolazak(@PathVariable Integer idk, HttpServletRequest request)
	{
		String email = (String) request.getAttribute("email");
		return servis.dodajDolazakS(idk, email);
	}
	
	@PostMapping("/add/{idk}/{idt}")
	public @ResponseBody DodajTreningResponseDto dodajTrening(@PathVariable("idk") Integer idk, @PathVariable("idt") Integer idt)
	{
		return servis.dodajTreningS(idk, idt);
	}
	
	@PostMapping("/kraj/{idk}")
	public @ResponseBody DodajOdlazakResponseDto dodajOdlazak(@PathVariable Integer idk)
	{
		return servis.dodajOdlazakS(idk);
	}
	
	@GetMapping("/all")
	public List<Dolazak> listajSveDolaske()
	{
		return servis.listajSveDolaskeS();
	}
	
	@GetMapping("/all/interval")
	public List<Dolazak> listajIzmedju(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime vreme1, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime vreme2)
	{
		return servis.listajIzmedjuS(vreme1, vreme2);
	}
	
	@GetMapping("/allkor/interval")
	public List<KorisnikDto> listajKorIzmedju(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime vreme1, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime vreme2)
	{
		return servis.listajKorIzmedjuS(vreme1, vreme2);
	}
	
	@GetMapping("/prihodi")
	public String prihodiSvi()
	{
		return servis.prihodiSviS();
	}
}