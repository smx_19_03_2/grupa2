package Mladen.Teretana.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import Mladen.Teretana.domain.Pretplata;
import Mladen.Teretana.domain.dto.PretplataDto;
import Mladen.Teretana.domain.dto.response.ObrisiPretplatuResponseDto;
import Mladen.Teretana.domain.dto.response.PretplatiResponseDto;
import Mladen.Teretana.service.PretplataService;

@RestController
@RequestMapping("/pretplate")
public class PretplataController 
{
	@Autowired
	private PretplataService servis;
	
	@PostMapping("/add/{idk}")
	public @ResponseBody PretplatiResponseDto pretplati(@PathVariable Integer idk)
	{
		return servis.pretplatiS(idk);
	}
	
	@GetMapping("/all") // Opadajucim redosledom po datumu isticanja pretplate
	public @ResponseBody List<Pretplata> listajSve()
	{
		return servis.listajSveS();
	}
	
	@PostMapping("/all/update")
	public @ResponseBody List<PretplataDto> azurirajPretplate()
	{
		return servis.azurirajPretplateS();
	}
	
	@PostMapping("/delete/{idk}")
	public @ResponseBody ObrisiPretplatuResponseDto obrisiPretplatu(@PathVariable Integer idk)
	{
		return servis.obrisiPretplatuS(idk);
	}
}