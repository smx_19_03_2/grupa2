package Mladen.Teretana.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import Mladen.Teretana.domain.Korisnik;
import Mladen.Teretana.domain.dto.KorisnikDto;
import Mladen.Teretana.domain.dto.response.DodajKorisnikaResponseDto;
import Mladen.Teretana.domain.dto.response.ListajDolaskeZaIdResponseDto;
import Mladen.Teretana.domain.dto.response.NadjiIdKorisnikaResponseDto;
import Mladen.Teretana.service.KorisnikService;

@RestController
@RequestMapping("/korisnici")
public class KorisnikController 
{
	@Autowired
	private KorisnikService servis;
	
	@PostMapping("/add")
	public @ResponseBody DodajKorisnikaResponseDto dodajKorisnika(@RequestParam String ime, @RequestParam String prezime, @RequestParam String email)
	{
		return servis.dodajKorisnikaS(ime, prezime, email);
	}
	
	@GetMapping("/all")
	public @ResponseBody List<Korisnik> listajKorisnike()
	{
		return servis.listajKorisnikeS();
	}
	
	@GetMapping("/all/dolasci")
	public @ResponseBody List<KorisnikDto> listajKorisnikeDto()
	{
		return servis.listajKorisnikeDtoS();
	}
	
	@GetMapping("/{id}")
	public @ResponseBody NadjiIdKorisnikaResponseDto nadjiIdKorisnika(@PathVariable Integer id)
	{
		return servis.nadjiIdKorisnikaS(id);
	}
	
	@GetMapping("/") // Ne izbacuje poruku ako ne postoji prezime, nego vraca praznu listu
	public @ResponseBody List<KorisnikDto> nadjiPrezime(@RequestParam String prezime)
	{
		return servis.nadjiPrezimeS(prezime);
	}
	
	@GetMapping("/{id}/dolasci")
	public @ResponseBody ListajDolaskeZaIdResponseDto listajDolaskeZaId(@PathVariable Integer id)
	{
		return servis.listajDolaskeZaIdS(id);
	}
}