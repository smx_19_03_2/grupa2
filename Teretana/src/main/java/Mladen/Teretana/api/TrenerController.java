package Mladen.Teretana.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import Mladen.Teretana.domain.Trener;
import Mladen.Teretana.domain.dto.TrenerDto;
import Mladen.Teretana.domain.dto.response.DodajTreneraResponseDto;
import Mladen.Teretana.domain.dto.response.NadjiIdTreneraResponseDto;
import Mladen.Teretana.service.TrenerService;

@RestController
@RequestMapping("/treneri")
public class TrenerController 
{
	@Autowired
	private TrenerService servis;
	
	@PostMapping("/add")
	public @ResponseBody DodajTreneraResponseDto dodajTrenera(@RequestParam String ime, @RequestParam String prezime, @RequestParam Double cena)
	{
		return servis.dodajTreneraS(ime, prezime, cena);
	}
	
	@GetMapping("/all")
	public @ResponseBody List<Trener> listajTrenere()
	{
		return servis.listajTrenereS();
	}
	
	@GetMapping("/treninzi")
	public @ResponseBody List<TrenerDto> listajTreninge()
	{
		return servis.listajTreningeS();
	}
	
	@GetMapping("/{id}")
	public @ResponseBody NadjiIdTreneraResponseDto nadjiIdTrenera(@PathVariable Integer id)
	{
		return servis.nadjiIdTreneraS(id);
	}
}