package Mladen.Teretana.domain.dto;

import java.time.LocalDateTime;

public class TerminDto
{
	private String imeKorisnika;
	private String prezimeKorisnika;
	private int idTrenera;
	private LocalDateTime dolazak;
	private LocalDateTime odlazak;
	private double iznos;
	
	public String getImeKorisnika() {return imeKorisnika;}
	public void setImeKorisnika(String imeKorisnika) {this.imeKorisnika = imeKorisnika;}
	
	public String getPrezimeKorisnika() {return prezimeKorisnika;}
	public void setPrezimeKorisnika(String prezimeKorisnika) {this.prezimeKorisnika = prezimeKorisnika;}
	
	public int getIdTrenera() {return idTrenera;}
	public void setIdTrenera(int idTrenera) {this.idTrenera = idTrenera;}
	
	public LocalDateTime getDolazak() {return dolazak;}
	public void setDolazak(LocalDateTime dolazak) {this.dolazak = dolazak;}
	
	public LocalDateTime getOdlazak() {return odlazak;}
	public void setOdlazak(LocalDateTime odlazak) {this.odlazak = odlazak;}
	
	public double getIznos() {return iznos;}
	public void setIznos(double iznos) {this.iznos = iznos;}
}