package Mladen.Teretana.domain.dto.response;

public class DodajTreneraResponseDto
{
	private String ime;
	private String prezime;
	private double cena;
	
	public String getIme() {return ime;}
	public void setIme(String ime) {this.ime = ime;}
	
	public String getPrezime() {return prezime;}
	public void setPrezime(String prezime) {this.prezime = prezime;}
	
	public double getCena() {return cena;}
	public void setCena(double cena) {this.cena = cena;}
}