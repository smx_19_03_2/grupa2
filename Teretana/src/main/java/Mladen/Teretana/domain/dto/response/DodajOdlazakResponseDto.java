package Mladen.Teretana.domain.dto.response;

import Mladen.Teretana.domain.Dolazak;

public class DodajOdlazakResponseDto
{
	private boolean uspesnost;
	private String errorMessage;
	private Dolazak dolazak;
	
	public boolean isUspesnost() {return uspesnost;}
	public void setUspesnost(boolean uspesnost) {this.uspesnost = uspesnost;}
	
	public String getErrorMessage() {return errorMessage;}
	public void setErrorMessage(String errorMessage) {this.errorMessage = errorMessage;}
	
	public Dolazak getDolazak() {return dolazak;}
	public void setDolazak(Dolazak dolazak) {this.dolazak = dolazak;}
}