package Mladen.Teretana.domain.dto.response;

import Mladen.Teretana.domain.dto.TrenerDto;

public class NadjiIdTreneraResponseDto
{
	private boolean uspesnost;
	private String errorMessage;
	private TrenerDto trener;
	
	public boolean isUspesnost() {return uspesnost;}
	public void setUspesnost(boolean uspesnost) {this.uspesnost = uspesnost;}
	
	public String getErrorMessage() {return errorMessage;}
	public void setErrorMessage(String errorMessage) {this.errorMessage = errorMessage;}
	
	public TrenerDto getTrener() {return trener;}
	public void setTrener(TrenerDto trener) {this.trener = trener;}
}