package Mladen.Teretana.domain.dto;

import java.time.LocalDate;

public class VezbanjeDto
{
	private String imeKorisnika;
	private String prezimeKorisnika;
	private LocalDate datum;
	
	public String getImeKorisnika() {return imeKorisnika;}
	public void setImeKorisnika(String imeKorisnika) {this.imeKorisnika = imeKorisnika;}
	
	public String getPrezimeKorisnika() {return prezimeKorisnika;}
	public void setPrezimeKorisnika(String prezimeKorisnika) {this.prezimeKorisnika = prezimeKorisnika;}
	
	public LocalDate getDatum() {return datum;}
	public void setDatum(LocalDate datum) {this.datum = datum;}
}