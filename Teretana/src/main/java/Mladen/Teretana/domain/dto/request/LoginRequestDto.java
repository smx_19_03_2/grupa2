package Mladen.Teretana.domain.dto.request;

public class LoginRequestDto
{
	private String email;
	private String lozinka;
	
	public String getEmail() {return email;}
	public void setEmail(String email) {this.email = email;}
	
	public String getLozinka() {return lozinka;}
	public void setLozinka(String lozinka) {this.lozinka = lozinka;}
}