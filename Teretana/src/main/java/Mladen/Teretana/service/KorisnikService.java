package Mladen.Teretana.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Mladen.Teretana.dao.DolazakRepository;
import Mladen.Teretana.dao.KorisnikRepository;
import Mladen.Teretana.domain.Dolazak;
import Mladen.Teretana.domain.Korisnik;
import Mladen.Teretana.domain.dto.DolazakDto;
import Mladen.Teretana.domain.dto.KorisnikDto;
import Mladen.Teretana.domain.dto.response.DodajKorisnikaResponseDto;
import Mladen.Teretana.domain.dto.response.ListajDolaskeZaIdResponseDto;
import Mladen.Teretana.domain.dto.response.NadjiIdKorisnikaResponseDto;

@Service
public class KorisnikService 
{
	@Autowired
	private KorisnikRepository korisnikRepository;
	
	@Autowired
	private DolazakRepository dolazakRepository;
	
	public DodajKorisnikaResponseDto dodajKorisnikaS(String ime, String prezime, String email) 
	{
		Korisnik korisnik = new Korisnik();
		korisnik.setIme(ime);
		korisnik.setPrezime(prezime);
		korisnik.setEmail(email);
		korisnik.setTippret((byte) 0);
		korisnikRepository.save(korisnik);
		DodajKorisnikaResponseDto response = new DodajKorisnikaResponseDto();
		response.setIme(ime);
		response.setPrezime(prezime);
		response.setEmail(email);
		return response;
	}
	
	public List<Korisnik> listajKorisnikeS()
	{
		return korisnikRepository.findAll();
	}
	
	public List<KorisnikDto> listajKorisnikeDtoS()
	{
		List<Korisnik> lk = korisnikRepository.findAll();
		List<KorisnikDto> lkd = new ArrayList<KorisnikDto>();
		for (int i=0; i<lk.size(); i++)
		{
			Korisnik k = lk.get(i);
			List<Dolazak> ld = new ArrayList<Dolazak>();
			ld = k.getDolasci();
			List<DolazakDto> ldd = new ArrayList<DolazakDto>();
			for (int j=0; j<ld.size(); j++)
			{
				Dolazak d = ld.get(j);
				DolazakDto dd = new DolazakDto();
				dd.setDolazak(d.getDolazak());
				dd.setOdlazak(d.getOdlazak());
				dd.setIznos(d.getIznos());
				if (d.getTrener()!=null) dd.setId_trenera(d.getTrener().getIdtrenera());
				ldd.add(dd);
			}
			KorisnikDto kd = new KorisnikDto();
			kd.setId_korisnika(k.getIdkorisnika());
			kd.setIme(k.getIme());
			kd.setPrezime(k.getPrezime());
			kd.setDolasci(ldd);
			lkd.add(kd);
		}
		return lkd;
	}
	
	public NadjiIdKorisnikaResponseDto nadjiIdKorisnikaS(Integer id)
	{
		Optional<Korisnik> ok = korisnikRepository.findById(id);
		NadjiIdKorisnikaResponseDto response = new NadjiIdKorisnikaResponseDto();
		if (!ok.isPresent())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Ne postoji korisnik za uneti ID korisnika!");
			return response;
		}
		Korisnik korisnik = ok.get();
		response.setUspesnost(true);
		response.setKorisnik(korisnik);
		return response;
	}
	
	public List<KorisnikDto> nadjiPrezimeS(String prezime)
	{
		List<Korisnik> lk = korisnikRepository.findByPrezime(prezime);
		List<KorisnikDto> lkd = new ArrayList<KorisnikDto>();
		for (int i=0; i<lk.size(); i++)
		{
			Korisnik k = lk.get(i);
			KorisnikDto kd = new KorisnikDto();
			kd.setId_korisnika(k.getIdkorisnika());
			kd.setIme(k.getIme());
			kd.setPrezime(k.getPrezime());
			List<DolazakDto> ldd = new ArrayList<DolazakDto>();
			if (k.getDolasci().isEmpty()) kd.setDolasci(ldd);
			else
			{
				List<Dolazak> ld = new ArrayList<Dolazak>();
				ld = k.getDolasci();
				for (int j=0; j<ld.size(); j++)
				{
					Dolazak d = ld.get(j);
					DolazakDto dd = new DolazakDto();
					dd.setDolazak(d.getDolazak());
					dd.setOdlazak(d.getOdlazak());
					dd.setIznos(d.getIznos());
					if (d.getTrener()!=null) dd.setId_trenera(d.getTrener().getIdtrenera());
					ldd.add(dd);
				}
				kd.setDolasci(ldd);
			}
			lkd.add(kd);
		}
		return lkd;
	}
	
	public ListajDolaskeZaIdResponseDto listajDolaskeZaIdS(Integer id)
	{
		Optional<Korisnik> ok = korisnikRepository.findById(id);
		ListajDolaskeZaIdResponseDto response = new ListajDolaskeZaIdResponseDto();
		if (!ok.isPresent())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Ne postoji korisnik za uneti ID korisnika!");
			return response;
		}
		response.setUspesnost(true);
		List<Dolazak> ld = dolazakRepository.findByKorisnik(ok.get());
		List<DolazakDto> ldd = new ArrayList<DolazakDto>();
		for (int i=0; i<ld.size(); i++)
		{
			Dolazak d = ld.get(i);
			DolazakDto dd = new DolazakDto();
			dd.setDolazak(d.getDolazak());
			dd.setOdlazak(d.getOdlazak());
			dd.setIznos(d.getIznos());
			if (d.getTrener()!=null) dd.setId_trenera(d.getTrener().getIdtrenera());
			ldd.add(dd);
		}
		response.setDolasci(ldd);
		return response;
	}
}