package Mladen.Teretana.security;

import java.security.Key;
import java.util.Date;

import javax.crypto.spec.SecretKeySpec;

import org.springframework.stereotype.Service;

import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenService
{
	private final String SECRET = "0Vde_Treb@D@Sen@l@z1k0dDuz1ne32byte-a";
	private final long EXPIRATION_TIME = 86400000; // 24 sata
	
	public String napraviToken(String email)
	{
		SignatureAlgorithm algoritam = SignatureAlgorithm.HS256;
		byte[] tajna = SECRET.getBytes();
		Key kljuc = new SecretKeySpec(tajna, algoritam.getJcaName());
		long danasM = System.currentTimeMillis();
		Date sutra = new Date(danasM + EXPIRATION_TIME);
		String token = Jwts.builder().setSubject(email).setExpiration(sutra).signWith(kljuc, algoritam).compact();
		return token;
	}
	
	public String validirajToken(String token)
	{
		byte[] tajna = SECRET.getBytes();
		String email;
		try
		{
			email = Jwts.parser().setSigningKey(tajna).parseClaimsJws(token).getBody().getSubject();
		}
		catch (JwtException ex)
		{
			email = null;
		}
		return email;
	}
}