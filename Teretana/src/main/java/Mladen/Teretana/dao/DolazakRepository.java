package Mladen.Teretana.dao;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import Mladen.Teretana.domain.Dolazak;
import Mladen.Teretana.domain.Korisnik;
import Mladen.Teretana.domain.Trener;

@Repository
public interface DolazakRepository extends JpaRepository<Dolazak, Integer>
{
	public List<Dolazak> findByDolazakBetween(LocalDateTime vreme1, LocalDateTime vreme2);
	public List<Dolazak> findByDolazakBetweenOrderByKorisnik(LocalDateTime vreme1, LocalDateTime vreme2);
	public List<Dolazak> findByKorisnik(Korisnik k);
	public List<Dolazak> findFirstByKorisnikOrderByDolazakDesc(Korisnik k);
	public List<Dolazak> findAllByOrderByDolazakDesc();
	public Integer countByTrener(Trener trener);
}