package Mladen.Teretana.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import Mladen.Teretana.domain.Korisnik;
import Mladen.Teretana.domain.Pretplata;

@Repository
public interface PretplataRepository extends JpaRepository<Pretplata, Integer>
{
	public List<Pretplata> findFirstByKorisnikOrderByKrajDesc(Korisnik korisnik);
	public List<Pretplata> findByKraj(LocalDate kraj);
	public List<Pretplata> findAllByOrderByKrajDesc();
}