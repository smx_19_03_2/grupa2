/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Grupa1
 */
@Entity
@Table(name = "UCENIK")
public class Ucenik {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String ime, prezime;

    @ManyToMany(mappedBy = "ucenici")
//    @JoinTable(
//            name = "ROD_UCE", 
//            joinColumns
//            = @JoinColumn(name = "UCENIK_ID", referencedColumnName = "ID"),
//            inverseJoinColumns
//            = @JoinColumn(name = "RODITELJ_ID", referencedColumnName = "ID"))
    private Set<Roditelj> roditelji = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "odeljenje_id")
    private Odeljenje odeljenje;

    @ManyToMany
    @JoinTable(
            name = "OCENA",
            joinColumns
            = @JoinColumn(name = "UCENIK_ID", referencedColumnName = "ID"),
            inverseJoinColumns
            = @JoinColumn(name = "PREDMET_ID", referencedColumnName = "ID"))
    private Set<Predmet> predmeti = new HashSet<>();

    public Ucenik() {
    }

    public Ucenik(long id) {
        this.id = id;
    }

    public Ucenik(String ime, String prezime, Odeljenje odeljenje) {
        this.ime = ime;
        this.prezime = prezime;
        this.odeljenje = odeljenje;
    }

    public Ucenik(long id, String ime, String prezime, Odeljenje odeljenje) {
        this.id = id;
        this.ime = ime;
        this.prezime = prezime;
        this.odeljenje = odeljenje;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public Set<Roditelj> getRoditelji() {
        return roditelji;
    }

    public void setRoditelji(Set<Roditelj> roditelji) {
        this.roditelji = roditelji;
    }

    public Odeljenje getOdeljenje() {
        return odeljenje;
    }

    public void setOdeljenje(Odeljenje odeljenje) {
        this.odeljenje = odeljenje;
    }

    public Set<Predmet> getPredmeti() {
        return predmeti;
    }

    public void setPredmeti(Set<Predmet> predmeti) {
        this.predmeti = predmeti;
    }

    @Override
    public String toString() {
        return "Ucenik{" + "id=" + id + ", ime=" + ime + ", prezime=" + prezime + ", roditelji=" + roditelji + ", odeljenje=" + odeljenje + ", predmeti=" + predmeti + '}';
    }

}
