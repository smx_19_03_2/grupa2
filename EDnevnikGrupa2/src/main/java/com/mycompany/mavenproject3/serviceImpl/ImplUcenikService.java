/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.serviceImpl;

import com.mycompany.mavenproject3.service.UcenikService;
import com.mycompany.mavenproject3.repository.GodinaRepository;
import com.mycompany.mavenproject3.repository.UcenikRepository;
import com.mycompany.mavenproject3.model.GodinaTest;
import com.mycompany.mavenproject3.model.UcenikTest;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 *
 * @author Grupa1
 */
@Service
public class ImplUcenikService implements UcenikService {

    private UcenikRepository ucenikRepo;
    
    private GodinaRepository godinaRepo;

    @Autowired
    public void setUcenikRepo(UcenikRepository ucenikRepo) {
        this.ucenikRepo = ucenikRepo;
    }
    @Autowired
    public void setGodinaReepo (GodinaRepository godinaRepo){
    this.godinaRepo= godinaRepo;
    }

    @Override
    public Iterable<UcenikTest> listAllUsers() {

        return ucenikRepo.findAll();

    }

    @Override
    public UcenikTest saveUser(UcenikTest user) {
		return ucenikRepo.save(user);
    }

    @Override
    public void deleteUser(long id) {
       ucenikRepo.deleteById(id);
    }

    @Override
    public UcenikTest getUserById(long id) {
        UcenikTest u = ucenikRepo.findById(id).get();
        return u;
       // return ucenikRepo.findById(id).orElseThrow(() -> new EntityNotFoundException(id));
    }
    
    public UcenikTest promeniUcenika(UcenikTest u) {
        
        Optional<UcenikTest> ucenik = ucenikRepo.findById(u.getId());
        
        if (ucenik.isPresent()) {
            ucenik.get().setId(u.getId());
            ucenik.get().setIme(u.getIme());
            ucenik.get().setPrezime(u.getPrezime());
            return ucenikRepo.save(ucenik.get());
        } else
            return null;
    }

    @Override
    public Iterable<GodinaTest> listAllGodina() {
    return godinaRepo.findAll();
    }

  
 

}


