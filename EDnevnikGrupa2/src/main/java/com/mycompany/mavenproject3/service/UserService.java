package com.mycompany.mavenproject3.service;

import com.mycompany.mavenproject3.model.Korisnik;

public interface UserService {
	

        Long findByEmailAndPassword(String email, String lozinka);
        
        Korisnik findKorisnikByEmailAndPassword (String email, String lozinka);
}
