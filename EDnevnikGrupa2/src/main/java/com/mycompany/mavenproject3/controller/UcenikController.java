/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.controller;

import com.mycompany.mavenproject3.serviceImpl.ImplUcenikService;
import com.mycompany.mavenproject3.repository.UcenikRepository;
import com.mycompany.mavenproject3.service.UcenikService;
import com.mycompany.mavenproject3.model.GodinaTest;
import com.mycompany.mavenproject3.model.UcenikTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Grupa1
 */
@Controller
@RequestMapping(path = "/ucenik")
public class UcenikController {
    // This means that this class is a Controller
//@RequestMapping(path = "/demo") // This means URL's start with /demo (after Application path)

    @Autowired
    private UcenikRepository ucenikRepository;

    private UcenikService ucenikService;

    @Autowired
    public void setUcenikService(ImplUcenikService ucenikService) {
        this.ucenikService = ucenikService;
    }

    @PostMapping(path = "/dodaj")
    public @ResponseBody
    String addNewProba(@RequestParam String ime, @RequestParam String prezime, @RequestParam(value = "godinaId") long godinaId) {

        GodinaTest g = new GodinaTest();
        g.setId(godinaId);

        System.out.println("Godina je " + g.toString());
        UcenikTest u = new UcenikTest();
        u.setIme(ime);
        u.setPrezime(prezime);
        u.setGodina(g);
        System.out.println("Ucenik je " + u.toString());
//        Long x = u.getGodina().getId();
//        (Long) u.setGodina(x);
        ucenikRepository.save(u);
        return "Snimljen Učenik !";

    }

    @GetMapping(path = "/allUcenik")
    public ModelAndView allUcenik() {
        ModelAndView model = new ModelAndView("allUcenik");
        model.addObject("list", ucenikService.listAllUsers());
        return model;
    }

    @GetMapping(path = "/{id}/editUcenik")
    public ModelAndView editUcenik(@PathVariable("id") long id) {
        ModelAndView model = new ModelAndView("editUcenik");
        UcenikTest u = ucenikService.getUserById(id);
        model.addObject("ucenik", u);
        model.addObject("listGodina", ucenikService.listAllGodina());
        return model;

    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ModelAndView update(@RequestParam("id") long id,
            @RequestParam("ime") String ime, @RequestParam("prezime") String prezime) {
        UcenikTest u = ucenikService.getUserById(id);
        //    u.setId(u.getId());
        u.setIme(ime);
        u.setPrezime(prezime);
        System.out.println("Ucenik je " + u.toString());
        ucenikService.saveUser(u);
        return new ModelAndView("redirect:/index");
    }

    @RequestMapping(value = "/{id}/delete", method = RequestMethod.GET)
    public ModelAndView deleteUsers(@PathVariable long id) {
        ucenikService.deleteUser(id);
        return new ModelAndView("redirect:/index");
    }
}
