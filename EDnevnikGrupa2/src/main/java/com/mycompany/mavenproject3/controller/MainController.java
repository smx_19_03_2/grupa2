package com.mycompany.mavenproject3.controller;

import com.mycompany.mavenproject3.repository.GodinaRepository;
import com.mycompany.mavenproject3.repository.KorisnikRepository;
import com.mycompany.mavenproject3.service.UcenikService;
import com.mycompany.mavenproject3.model.GodinaTest;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



import org.springframework.web.servlet.ModelAndView;

@Controller
//@RequestMapping(path = {"/index" } )
public class MainController {

    @Autowired
    private GodinaRepository godinaRepository;

    @Autowired
    private UcenikService ucenikService;
    
      @Autowired
    private KorisnikRepository korisnikRepository;

    @GetMapping(path = {"/", "/index"})
    public ModelAndView index() {
        ModelAndView model = new ModelAndView("index");
        model.addObject("listGodina", ucenikService.listAllGodina());


        List<GodinaTest> godinaTestLista = godinaRepository.FindAllGodinaTest(2L);
        for (GodinaTest g : godinaTestLista) {
            System.out.println(g);
        }
        return model;

    }
    
    
        @RequestMapping(value = {"/admin"}, method = RequestMethod.GET)
    public String admin(Model model) {

        return "admin";
    }

    @RequestMapping(value = {"/direktor"}, method = RequestMethod.GET)
    public String direktor(Model model) {

        return "direktor";
    }

    @RequestMapping(value = {"/roditelj"}, method = RequestMethod.GET)
    public String roditelj(Model model) {

        return "roditelj";
    }

    @RequestMapping(value = {"/ucitelj"}, method = RequestMethod.GET)
    public String ucitelj(Model model) {

        return "ucitelj";
    }

    @RequestMapping(value = {"/komunikacija"}, method = RequestMethod.GET)
    public String komunikacija(Model model) {

        return "komunikacija";
    }
}

//  @GetMapping(path = "/allUcenik")
//    public ModelAndView allUcenik() {
//        ModelAndView model = new ModelAndView("allUcenik");
//        model.addObject("list", ucenikService.listAllUsers());
//        return model;