/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.controller;

import com.mycompany.mavenproject3.model.Korisnik;
import com.mycompany.mavenproject3.repository.KorisnikRepository;
import com.mycompany.mavenproject3.repository.OdeljenjeRepository;
import com.mycompany.mavenproject3.repository.UcenikRepository;
import com.mycompany.mavenproject3.service.UcenikService;
import com.mycompany.mavenproject3.serviceImpl.ImplUcenikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Grupa1
 */

@Controller
@RequestMapping(path = "/Odeljenje")
public class UciteljController {
    
    
     @Autowired
    private UcenikRepository ucenikRepository;
     @Autowired
     private OdeljenjeRepository odeljenjeRepository;
       @Autowired
     private KorisnikRepository korisnikRepository;

    private UcenikService ucenikService;

    @Autowired
    public void setUcenikService(ImplUcenikService ucenikService) {
        this.ucenikService = ucenikService;
    }
    
    
    @ResponseBody
   
    public ModelAndView odeljenje(@RequestParam String email, @RequestParam String lozinka) {
        ModelAndView model = new ModelAndView("allUcenik");
        //ubaci listu sa ucenicima ali napravi novi metod
        Korisnik k = korisnikRepository.findKorisnik(email, lozinka);
        Long id = k.getId();
        model.addObject("list", ucenikService.listAllUsers());
    //    model.addObject("odeljenje", odeljenjeRepository.findOdeljenjeByKorisnikId(id));
        return model;
    }
}
