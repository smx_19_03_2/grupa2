package com.mycompany.mavenproject3.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import com.mycompany.mavenproject3.model.Korisnik;
import org.springframework.data.repository.query.Param;

@Repository
public interface KorisnikRepository extends JpaRepository<Korisnik, Long> {
	

	@Query("select k.funkcija.id from Korisnik k where k.email= :email and k.lozinka= :lozinka")
	public Long findFunkcija(@Param("email") String email, @Param("lozinka") String lozinka);
        
        
        @Query ("select k from Korisnik k where k.email= :email and k.lozinka= :lozinka")
        public Korisnik findKorisnik (@Param("email") String email, @Param("lozinka") String lozinka);
        
}
