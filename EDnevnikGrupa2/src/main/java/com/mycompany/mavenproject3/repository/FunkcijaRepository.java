package com.mycompany.mavenproject3.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mycompany.mavenproject3.model.Funkcija;;

public interface FunkcijaRepository extends JpaRepository<Funkcija, Long> {

}
