/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.repository;

import com.mycompany.mavenproject3.model.GodinaTest;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Grupa1
 */
public interface GodinaRepository extends CrudRepository<GodinaTest, Long>{
    
	@Query("select g from GodinaTest g where id=?1")
	public List<GodinaTest> FindAllGodinaTest(long id);

     
}
