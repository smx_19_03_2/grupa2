<%-- 
    Document   : Odeljenje
    Created on : Mar 27, 2019, 10:42:23 AM
    Author     : Grupa1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>Odeljenje</title>

            <link href="/webjars/bootstrap/4.3.0/css/bootstrap.min.css"
                  rel="stylesheet">
            <link rel="stylesheet" type="text/css" href="/style.css">
        </head>
        <body>
            <h1><h:outputText value="Pregled odeljenja"/></h1>

            <p>${odeljenje.id}</p>
            <p>${odeljenje.naziv}</p>
            

            <script src="/webjars/jquery/3.2.0/jquery.min.js"></script>
            <script src="/webjars/bootstrap/4.3.0/js/bootstrap.min.js"></script>
            <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
            <script src="/design.js"></script>
        </tbody>

    </body>
</html>
</f:view>
