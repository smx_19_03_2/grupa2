/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.controllers;

import com.mycompany.mavenproject3.model.Korisnik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.mycompany.mavenproject3.repositories.KorisnikRepository;
import com.mycompany.mavenproject3.services.AdminService;
import com.mycompany.mavenproject3.services.AdminServiceImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Grupa1
 */
@Controller
@RequestMapping(path = "/admin")
public class AdminController {
    
    @Autowired
    private KorisnikRepository korisnikRepository;
    private AdminService adminService;
    
    @Autowired
    public void setAdminService(AdminServiceImpl adminService) {
        this.adminService = adminService;
    }
    
    @PostMapping(path = "/dodajKorisnik")
    public @ResponseBody
    String addNewKorisnik(@RequestParam String ime, @RequestParam String prezime, @RequestParam String email, @RequestParam String lozinka) {
        
//        Godina g = new Godina();
//        g.setId(godinaId);

        Korisnik k = new Korisnik();
        k.setIme(ime);
        k.setPrezime(prezime);
        k.setEmail(email);
        k.setLozinka(lozinka);
//        Long x = u.getGodina().getId();
//        (Long) u.setGodina(x);
        korisnikRepository.save(k);
        return "Snimljen Učenik !";
    }
    
    @GetMapping(path = "/allUloga")
    public ModelAndView allUloga() {
        ModelAndView model = new ModelAndView("allUloga");
        model.addObject("listUloga", adminService.listAllUsers());
        return model;
    }
}
