/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Grupa1
 */
@Controller

public class MainController {
    
    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
	public String index(Model model) {
		
		return "index";
	}
   @RequestMapping(value = {"/admin" }, method = RequestMethod.GET)
	public String admin(Model model) {
		
		return "admin";
	}
        
        @RequestMapping(value = {  "/adminKorisnik" }, method = RequestMethod.GET)
	public String adminKorisnik(Model model) {
		
		return "adminKorisnik";
	}
}
