/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.services;

import com.mycompany.mavenproject3.model.Korisnik;
import com.mycompany.mavenproject3.model.Uloga;
import java.util.Optional;
import com.mycompany.mavenproject3.repositories.KorisnikRepository;
import com.mycompany.mavenproject3.repositories.UlogaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Grupa1
 */
@Service
public class AdminServiceImpl implements AdminService {

    private KorisnikRepository korisnikRepo;
    
    private UlogaRepository ulogaRepo;
    
    @Autowired
    public void setKorisnikRepository(KorisnikRepository korisnikRepository) {
        this.korisnikRepo = korisnikRepo;
    }
    
    @Override
    public Iterable<Korisnik> listAllUsers() {
       return korisnikRepo.findAll();
    }

    @Override
    public Korisnik getUserById(long id) {
        Korisnik k = korisnikRepo.findById(id).get();
        return k;
    }

    @Override
    public Korisnik saveUser(Korisnik korisnik) {
        return korisnikRepo.save(korisnik);
    }

    @Override
    public void deleteUser(long id) {
        korisnikRepo.deleteById(id);
    }

    
    @Override
    public Korisnik promeniUcenika(Korisnik k) {
        Optional<Korisnik> korisnik;
        korisnik = korisnikRepo.findById(k.getId());
        
        if (korisnik.isPresent()) {
            korisnik.get().setId(k.getId());
            korisnik.get().setIme(k.getIme());
            korisnik.get().setPrezime(k.getPrezime());
            return korisnikRepo.save(korisnik.get());
        } else
            return null;
    }

    @Override
    public Iterable<Uloga> listAllUloga() {
        return ulogaRepo.findAll();
    }
    }
    

