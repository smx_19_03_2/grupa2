/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3.services;

import com.mycompany.mavenproject3.model.Korisnik;
import com.mycompany.mavenproject3.model.Uloga;

/**
 *
 * @author Grupa1
 */
public interface AdminService {
    
    public Iterable<Korisnik> listAllUsers();

	   public Korisnik getUserById(long id);

	   public Korisnik saveUser(Korisnik korisnik);
	    
	   public void deleteUser(long id);
           
           public Korisnik promeniUcenika (Korisnik k);
           
           public Iterable <Uloga> listAllUloga();
}
