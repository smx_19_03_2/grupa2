<%-- 
    Document   : index
    Created on : Mar 14, 2019, 10:56:34 AM
    Author     : Grupa1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>E DNEVNIK</title>
            <link href="/webjars/bootstrap/4.3.0/css/bootstrap.min.css"
              rel="stylesheet">
            <link rel="stylesheet" type="text/css" href="/style.css">
        </head>
        <body>
            <div id="wrapper">
                <div class="container" style="margin-top: 100px">
                    <form name="send" action="/admin/findByEmailAndPassword" method="POST"> 
                        <div class="form-group">
                        <label for="exampleInputEmail1">Email address:</label> <input
                            type="email" name="email" class="form-control" id="exampleInputEmail1"
                            aria-describedby="emailHelp" placeholder="Enter email"> <small
                            id="emailHelp" class="form-text text-muted">We'll never
                            share your email with anyone else.</small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password:</label> <input
                                type="password" name="lozinka" class="form-control" id="exampleInputPassword1"
                                placeholder="Password">
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Check me
                                out</label>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </body>
    </html>
</f:view>
