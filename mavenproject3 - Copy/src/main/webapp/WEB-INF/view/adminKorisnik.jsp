<%-- 
    Document   : adminKorisnik
    Created on : Mar 19, 2019, 11:06:21 AM
    Author     : Grupa1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>ADMIN</title>
            <link href="/webjars/bootstrap/4.3.0/css/bootstrap.min.css"
                  rel="stylesheet">
            <link rel="stylesheet" type="text/css" href="/style.css">
        </head>
        <body>
            <div id="wrapper">
                <div class="container" style="margin-top: 100px">
                    <form action="/admin/dodajKorisnik" method="post">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Ime:</label> <input
                                type="text" name="ime" class="form-control" id="ime"
                                aria-describedby="emailHelp">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Prezime:</label> <input
                                type="text" name="prezime" class="form-control" id="prezime"
                                aria-describedby="emailHelp">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email:</label> <input
                                type="email" name="email" class="form-control" id="exampleInputEmail1"
                                aria-describedby="emailHelp">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Lozinka:</label> <input
                                type="password" name="lozinka" class="form-control" id="exampleInputPassword1">
                        </div>

                        <br><hr>
                        <button type="submit" class="btn btn-primary">Ubaci</button>
                    </form>
                </div>
            </div>
        </body>
    </html>
</f:view>
