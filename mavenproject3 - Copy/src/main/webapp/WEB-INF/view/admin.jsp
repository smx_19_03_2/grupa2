<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
    <head>
        <title>Strana za administratora</title>
        <link href="/webjars/bootstrap/4.3.0/css/bootstrap.min.css"
              rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/style.css">
    </head>
    <body>
        <div id="wrapper">

            <ul id="tabs">
                <li><a href="#" name="tab1">Korisnici</a></li>
                <li><a href="#" name="tab2">Ucenici</a></li>
                <li><a href="#" name="tab3">Predmeti</a></li>
                <li><a href="#" name="tab4">Raspored</a></li>
                <li><a href="#" name="tab5">Obavestenja</a></li>    
            </ul>        

            <div id="content">                
                <div id="tab1">
                    <div class="container">
                        <form action="/admin/dodajKorisnik" method="post">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Ime:</label> <input
                                type="text" name="ime" class="form-control" id="ime"
                                aria-describedby="emailHelp">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Prezime:</label> <input
                                type="text" name="prezime" class="form-control" id="prezime"
                                aria-describedby="emailHelp">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email:</label> <input
                                type="email" name="email" class="form-control" id="exampleInputEmail1"
                                aria-describedby="emailHelp">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Lozinka:</label> <input
                                type="password" name="lozinka" class="form-control" id="exampleInputPassword1">
                        </div>
                        <div class="form-group">
                            <label>Izaberite ulogu:</label>
                            <select name="ulogaId">
                                <c:forEach items="${listUloga}" var="lu">
                                    <option  value="<c:out value="${lu.id}"></c:out>">${lu.naziv}</option>
                                </c:forEach>
                            </select>
                            <br><br>
                        </div>
                            
                        <br><hr>
                        <button type="submit" class="btn btn-primary">Ubaci</button>
                    </form>
                    </div>
                </div>  <!-- end of tab1-->
                <div id="tab2">
                    <div class="container">
                        <form>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Ime:</label> <input
                                    type="email" class="form-control" id="ime"
                                    aria-describedby="emailHelp">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Prezime:</label> <input
                                    type="email" class="form-control" id="prezime"
                                    aria-describedby="emailHelp">
                            </div>
                            <div class="form-group">
                                <label>Odeljenje: </label>                                
                                <select name="item">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="3">4</option>
                                    <option value="3">5</option>
                                </select>

                            </div>
                            <br>
                            <hr>
                            <button type="submit" class="btn btn-primary">Ubaci</button>
                        </form>
                    </div>
                </div> <!-- end of tab2-->

                <div id="tab3">
                    <div class="container" id="manipulacijaPredmetima">
                        <div id="izborOdeljenja">
                            <p>Izaberi Odeljenje: </p>                                
                            <select name="item">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="3">4</option>
                                <option value="3">5</option>
                            </select>
                        </div>

                        <div class="predmeti">
                            <table>
                                <caption>Spisak predmeta</caption>
                                <tr>
                                    <td>1</td>
                                    <td>predmet</td> 
                                    <td><button>Promeni</button></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>predmet</td> 
                                    <td><button>Promeni</button></td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>predmet</td> 
                                    <td><button>Promeni</button></td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>predmet</td> 
                                    <td><button>Promeni</button></td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>predmet</td> 
                                    <td><button>Promeni</button></td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>predmet</td> 
                                    <td><button>Promeni</button></td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>predmet</td> 
                                    <td><button>Promeni</button></td>
                                </tr>
                                <tr>
                                    <td>8</td>
                                    <td>predmet</td> 
                                    <td><button>Promeni</button></td>
                                </tr>
                                <tr>
                                    <td>9</td>
                                    <td>predmet</td> 
                                    <td><button>Promeni</button></td>
                                </tr>
                                <tr>
                                    <td>10</td>
                                    <td>predmet</td> 
                                    <td><button>Promeni</button></td>
                                </tr>
                            </table>
                        </div>
                        <br><br>
                        <button type="submit" class="btn btn-primary">Promeni</button>
                    </div>
                </div> <!-- end of tab3-->

                <div id="tab4">
                    <div class="container" id="manipulacijaRasporedom">
                        <div id="izborOdeljenja">
                            <p>Izaberi Odeljenje: </p>                                
                            <select name="item">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="3">4</option>
                                <option value="3">5</option>
                            </select>
                        </div>
                        <br><br>

                        <table class="rasporedCasova">
                            <caption>Raspored casova</caption>
                            <tr>
                                <th>redni br.</th>
                                <th>Ponedeljak</th> 
                                <th>Utorak</th>
                                <th>Sreda</th>
                                <th>Cetvrtak</th> 
                                <th>Petak</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>predmet</td> 
                                <td>ajsdhasjdjashd</td>
                                <td>sjkdhajdh</td>
                                <td>predmet</td> 
                                <td>jcjasdasjd</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>predmet</td> 
                                <td>sfsjkdhfjsdhf</td>
                                <td>sdhhsjdhsjdh</td>
                                <td>predmet</td> 
                                <td>sdfhsjdhfjsdfh</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>predmet</td> 
                                <td>kjhgfdfg</td>
                                <td>sjhdjshjsdh</td>
                                <td>predmet</td> 
                                <td>kjhgfdsgf</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>predmet</td> 
                                <td>sdfsdfghgfd</td>
                                <td>sjhdjshjshd</td>
                                <td>predmet</td> 
                                <td>sdfsdfsfsd</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>predmet</td> 
                                <td>sdfdfsdfsd</td>
                                <td>sdfsfsfsd</td>
                                <td>predmet</td> 
                                <td>sdfsdfsdfsd</td>
                            </tr>
                        </table>
                        <br><br>
                        <button type="submit" class="btn btn-primary">Promeni</button>
                    </div>
                </div> <!-- end of tab4-->

                <div id="tab5">
                    <div class="container" id="dodavanjeKorisnika">
                        <form>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Ime:</label> <input
                                    type="email" class="form-control" id="ime"
                                    aria-describedby="emailHelp" placeholder="New First Name">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Prezime:</label> <input
                                    type="email" class="form-control" id="prezime"
                                    aria-describedby="emailHelp" placeholder="New Last Name">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email:</label> <input
                                    type="email" class="form-control" id="exampleInputEmail1"
                                    aria-describedby="emailHelp" placeholder="New email">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Sifra:</label> <input
                                    type="password" class="form-control" id="exampleInputPassword1"
                                    placeholder="New Password">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Ponovite sifru:</label> <input
                                    type="password" class="form-control" id="exampleInputPassword1"
                                    placeholder="Repeat New Password">
                            </div>
                            <br>
                            <hr>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div> <!-- end of tab5-->

            </div>

            <script src="/webjars/jquery/3.2.0/jquery.min.js"></script>
            <script src="/webjars/bootstrap/4.3.0/js/bootstrap.min.js"></script>
            <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
            <script src="/design.js"></script>
        </div>  <!-- end of wrapper-->
    </body>
</html>
